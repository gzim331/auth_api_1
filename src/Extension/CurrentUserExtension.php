<?php

namespace App\Extension;

use ApiPlatform\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class CurrentUserExtension implements QueryItemExtensionInterface
{
    public function __construct(private readonly TokenStorageInterface $tokenStorage)
    {
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, Operation $operation = null, array $context = []): void
    {
        if (User::class === $resourceClass && 'current_user' === $operation->getName() && $this->tokenStorage->getToken()) {
            $this->support($queryBuilder);
            dump($this->tokenStorage->getToken());
        }
    }

    private function support(QueryBuilder $queryBuilder): void
    {
        $user = $this->tokenStorage->getToken()->getUserIdentifier();

        $rootAlias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->andWhere(sprintf('%s.email = :current_user_email', $rootAlias));
        $queryBuilder->setParameter('current_user_email', $user);
    }
}