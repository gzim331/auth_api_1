<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'load:user',
)]
class LoadUserCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private UserPasswordHasherInterface     $passwordHasher,
        string                                  $name = null
    )
    {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Create user admin');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'User Creator',
            '- - - - - - -',
            '',
        ]);

        $plain_email = $_ENV['DEFAULT_EMAIL'];
        $plain_password = $_ENV['DEFAULT_PASSWORD'];

        if ($user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $plain_email])) {
            $user->setPassword($this->passwordHasher->hashPassword(
                $user,
                $plain_password
            ));
        } else {
            $user = (new User())
                ->setEmail($plain_email)
                ->setRoles(['ROLE_USER']);

            $user->setPassword($this->passwordHasher->hashPassword(
                $user,
                $plain_password
            ));

            $this->entityManager->persist($user);

        }

        $this->entityManager->flush();

        $output->writeln([
            'User admin was created'
        ]);

        return Command::SUCCESS;
    }
}
