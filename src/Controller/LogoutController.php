<?php

namespace App\Controller;

use App\Entity\RefreshToken;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authenticator\JWTAuthenticator;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface as CsrfTokenStorageInterface;

class LogoutController extends AbstractController
{
    public function __construct(
        private RequestStack              $requestStack,
        private TokenStorageInterface     $tokenStorage,
        private CsrfTokenStorageInterface $csrfTokenStorage,
        private EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/api/logout', name: 'app_logout')]
    public function logout(): Response
    {
        $token = $this->tokenStorage->getToken();

        if ($token !== null) {
            // Invalidate the user's token to log them out
            $this->tokenStorage->setToken(null);
        }

        // Get the CSRF token id from the current request
        $csrfTokenId = $this->requestStack->getCurrentRequest()->get('_csrf_token_id');

        if ($csrfTokenId !== null) {
            // Remove the CSRF token using its id
            $this->csrfTokenStorage->removeToken($csrfTokenId);
        }

        foreach ($this->entityManager->getRepository(RefreshToken::class)->findBy(['username' => $token->getUserIdentifier()]) as $item) {
            $this->entityManager->remove($item);
        }
        $this->entityManager->flush();

        return new Response();
    }
}
